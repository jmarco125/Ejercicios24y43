
/* 43) (Rendimiento del auto) Los automovilistas se encuentran preocupados 
por el consumo y rendimiento de su vehículo.
Algunos de ellos llevan un registro con varias cargas de combustible, 
el costo del mismo y la cantidad de kilómetros recorridos con esa cantidad.

Escriba un programa que permita al usuario ingresar dicha información 
y muestre el rendimiento (en litros / 100 km) del vehículo cada vez. 
Al finalizar, imprima el rendimiento promedio, el mínimo, el máximo, 
el total de kilómetros recorridos y el dinero utilizado.*/

#include <stdio.h>

#define KM_RECORRIDOS "Ingrese los Km recorridos:"
#define COMBUSTIBLE_LITRO "Ingrese el precio del combustible por litro:"
#define MONTO_COMPRA "Ingrese el monto de esta compra:"
#define RENDIMIENTO "El rendimiento (en litros / 100 km) del vehículo es:"
#define MSJ_OPCION "Pecione 1 para ingresar mas datos o cualquier numero para terminar. "
#define RENDIM_PROM "El rendimiento promedio es: "
#define RENDIM_MAYOR "Mejor Rendimiento"
#define RENDIM_MENOR "Peor Rendimiento"
#define DISTANCIA "La distancia total recorrida es (Km):"
#define COMBUSIBLE_CONS "Combustible total consumido (L): "
#define COSTO_TOTAL "Costo total del automovil: "
#define SEPARAR " *********************************************************** "
#define MSJ_ERROR "ERROR, verifique el numero ingresado."

int main ()
{

	float km, precio, monto, litros, rendimiento, mayor=0, totalitro;
	float totalkm, totalmonto, totalrend=0, menor=0;
	int opcion, i=0, x;

		do{
	
			printf("%s", KM_RECORRIDOS);
			x=scanf("%f",&km);

			while(x!=1)
				{
				
				fprintf(stderr, "%s\n",MSJ_ERROR );
			
				return 1;		
				}

				totalkm=totalkm+km;

			printf("%s",COMBUSTIBLE_LITRO );
			x=scanf("%f",&precio);
			while(x!=1)
				{
				
				fprintf(stderr, "%s\n",MSJ_ERROR );
			
				return 1;		
				}

			printf("%s", MONTO_COMPRA);
			x=scanf("%f",&monto);
			while(x!=1)
				{
				
				fprintf(stderr, "%s\n",MSJ_ERROR );
			
				return 1;		
				}
				totalmonto=totalmonto+monto;

				litros=(monto/precio);
				totalitro=totalitro+litros;
				rendimiento=((litros/km)*100);
				totalrend=totalrend+rendimiento;
				i=i+1;

				if(i==1)
					mayor=rendimiento;
					menor=rendimiento;

				if(mayor<=rendimiento)
					mayor=rendimiento;

				if (menor>=rendimiento)
					menor=rendimiento;
	
	
			printf("%s %.2f \n", RENDIMIENTO, rendimiento);
			printf("%s\n",MSJ_OPCION);
			scanf("%d",&opcion);

		}while(opcion==1);

			printf("%s\n", SEPARAR);
			printf("%s %.02f\n", RENDIM_PROM, totalrend/i );
			printf("%s %.02f\n", RENDIM_MAYOR, mayor);
			printf("%s %.02f\n", RENDIM_MENOR, menor);
			printf("%s %.2f\n", DISTANCIA, totalkm);
			printf("%s %.2f\n", COMBUSIBLE_CONS, totalitro);
			printf("%s %.2f\n", COSTO_TOTAL, totalmonto);
			printf("%s\n", SEPARAR);

return 0;
}