
/*24) Escribir un programa que dado un día del año (1 a 366) ingresado por el usuario, 
imprima el día de la semana que le corresponde. Debe asumir que el año comenzó, 
por ejemplo, un domingo. Por ejemplo: si se ingresa '5', imprime 'jueves'
si se ingresa '10' imprime 'martes', si se ingresa '294' imprime 'sabado'.*/

#include <stdio.h>
#include <stdlib.h>

#define INGRESE_NUM "Ingrese un numero entero entre 1 y 366"
#define MSJ_ERROR "Verifique que el numero ingresado sea correcto"
#define DIA_1 "LUNES"
#define DIA_2 "MARTES"
#define DIA_3 "MIERCOLES"
#define DIA_4 "jUEVES"
#define DIA_5 "VIERNES"
#define DIA_6 "SABADO"
#define DIA_7 "DOMINGO"


 int main()
 {
 	int b,a,x;

 	printf("%s\n",INGRESE_NUM);

	x=scanf("%d",&b);

	while(x!=1)
	{
		fprintf(stderr, "%s\n",MSJ_ERROR );
		return 1;		
	}
 
 	while(b>366 || b<1)

 	{

 		printf("%s\n",MSJ_ERROR);
		printf("%s\n",INGRESE_NUM);
		scanf("%d",&b);
 	}
 	
 			a=b%7;

 		switch (a)
 		{
 			case 0:
 				printf("%s\n",DIA_6 );

 			break;

 			case 1:
 				printf("%s\n",DIA_7 );
 			break;

 			case 2:
 				printf("%s\n",DIA_1 );
 			break;

 			case 3:
 				printf("%s\n",DIA_2 );
 			break;

 			case 4:
 				printf("%s\n",DIA_3 );
 			break;

 			case 5:
 				printf("%s\n",DIA_4 );
 			break;

 			case 6:
 				printf("%s\n",DIA_5 );
 			break;

 		}

 		
	return 0;
 } 	
